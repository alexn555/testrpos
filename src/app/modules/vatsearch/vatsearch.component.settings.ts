export const MIN_SEACH_LENGTH = 5;
export const CHECK_DUPLICATE = true;
export const CHECK_HUMAN = false;

export enum SEARCH_ERROR_CASES {
  minLength = 'minLength',
  nothingFound = 'nothingFound'
}
