import {Component, OnInit, NgZone} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

import {Router} from '@angular/router';
import {VatSearchService} from './vatsearch.service';
import {VatResult} from './vatsearch.model';
import { MIN_SEACH_LENGTH, CHECK_DUPLICATE,
        CHECK_HUMAN, SEARCH_ERROR_CASES } from './vatsearch.component.settings';

@Component({
  selector: 'app-vat-search',
  templateUrl: './vatsearch.component.html',
  styleUrls: ['./vatsearch.component.scss']
})
export class VatSearchComponent implements OnInit {

  public vatSeachForm: FormGroup;
  public vatResultError: boolean;
  public vatResultSearchStarted: boolean;
  public vatResult: VatResult;

  public vatSearchIsHuman: boolean;
  public siteKeyCaptcha = '6LeEM34UAAAAACvl5VSbN8uJBSE3B88-8r-BJQ4-';

  // settings
  public isSettingsOpen: boolean;
  public vatLastSearchesVisible = true;
  public vatSeatchTextClass = 'vat-search-text-upper';

  // last successful search results to quickly type to input box
  public vatLastSearches = [];
  public vatResultErrorMessage: string;

  constructor(private fb: FormBuilder,
              private zone: NgZone,
              private translate: TranslateService,
              private router: Router,
              private service: VatSearchService) {
    this.createSearchForm();
    translate.setDefaultLang('en');
    translate.use('en');
  }

  ngOnInit(): void {
    this.initRecaptcha();
  }

  createSearchForm(): void {
    this.vatSeachForm = this.fb.group({
        vatNumber: ['', [Validators.required, Validators.minLength(MIN_SEACH_LENGTH)]],
      }
    );
    this.vatSeachForm = new FormGroup(this.vatSeachForm.controls, {
      updateOn: 'blur'
    });
  }

  initRecaptcha() {
    if (CHECK_HUMAN) {
      window['getResponceCapcha'] = this.getResponceCapcha.bind(this);
    } else {
      this.vatSearchIsHuman = true;
    }
  }

  getResponceCapcha(captchaResponse: string) {
    this.vatSearchIsHuman = this.verifyCaptcha(captchaResponse);
    this.zone.run(() => { });
  }

  verifyCaptcha(captchaResponse: string) {
    return captchaResponse && captchaResponse.length > 20;
  }


  searchVATNumber() {
    this.vatResultSearchStarted = true;
    this.resetSearch();
    const searchVAT = this.vatSeachForm.value.vatNumber.toUpperCase();
    if (searchVAT && searchVAT.length >= MIN_SEACH_LENGTH) {

      this.service.getVATResult(searchVAT).subscribe((data) => {
        if (data && data !== null) {
          this.showSuccess(data);
          this.addNewLastSearch(searchVAT);
        } else {
          this.showError(SEARCH_ERROR_CASES.nothingFound);
        }
      }, error => {
        this.showError(SEARCH_ERROR_CASES.nothingFound);
      });

    } else {
       this.showError(SEARCH_ERROR_CASES.minLength);
    }

  }

  private showError(condition: SEARCH_ERROR_CASES) {
    this.vatResultSearchStarted = false;
    this.vatResultError = true;
    switch (condition) {
      case SEARCH_ERROR_CASES.minLength:
        this.vatResultErrorMessage = this.translate.instant('vat-search.minChars');
        break;
      case SEARCH_ERROR_CASES.nothingFound:
      default:
        this.vatResultErrorMessage = this.translate.instant('vat-search.commonError');
    }
  }

  private showSuccess(result) {
    this.vatResultSearchStarted = false;
    this.vatResult = {
      country: result.CountryCode,
      name: result.Name,
      address: result.Address
    };
  }

  private resetSearch() {
    this.vatResultError = false;
  }

  private addNewLastSearch(vatNumber: string) {
    if (!this.checkIfDuplicateVatNumber(vatNumber)) {
      if (this.vatLastSearches.length > 2) {
        this.vatLastSearches.splice(0, 1);
      }
      this.vatLastSearches.push(vatNumber);
    }
  }

  private checkIfDuplicateVatNumber(vatNumber: string) {
    return CHECK_DUPLICATE ? this.vatLastSearches.indexOf(vatNumber) > -1 : false;
  }

  addToInputField(vatNumber: string) {
    this.vatSeachForm.setValue({
      vatNumber: vatNumber
    });
  }

  openAboutPage() {
    this.router.navigate(['about']);
  }

  onSettingsChanged(settingsData: any) {
    if (settingsData) {
      this.vatLastSearchesVisible = settingsData.lastSearches;
      if (settingsData.upperCaseSearchText === true) {
        this.vatSeatchTextClass = 'vat-search-text-upper';
      } else {
        this.vatSeatchTextClass = 'vat-search-text-lower';
      }
    }
  }

  clearErrorMessage() {
    this.vatResultError = false;
    this.vatResultErrorMessage = '';
  }

  toggleSettings() {
    this.clearErrorMessage();
    this.isSettingsOpen = !this.isSettingsOpen;
  }


}
