import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {VatResultRaw} from './vatsearch.model';

@Injectable()
export class VatSearchService {
  url: string;

  constructor(private http: HttpClient) {
    this.url = 'https://vat.erply.com/';
  }

  getVATResult(vatNumber: string): Observable<VatResultRaw> {
    return this.http.get<VatResultRaw>(this.url + 'numbers?vatNumber=' + vatNumber);
  }

}

