# Vatchecker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.0.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
```
## Installation and usage

 1. Copy project folder to some projects location
 2. Use command line and nodejs, install node modules, cmd ->  cd project-location,  npm install 
 3. Once installed to start application use cmd -> npm run start 
 4. Type localhost:4200 in browser 
 5. Type VAT number and press search, you should see correct result (or error if not typed or result not found)
 6. You can use last search results as quick type links
 
 Main structure 
 
   src / 
     app / 
      modules /
	  
	   common /
	    header / -> header section of application
		footer / -> footer section of application  
		loading-bar / -> bar that showing loading icon
		themes / - common, specific theme scss components that are common for application
		
		vatsearch /  ->  component that uses Angular classical structure and searches for VAT information
		  .component, html, scss, service 
		  settings / -> settings component of vatsearch
		   
       app.ts -> main application, starting point of application
	   
	  Currently modules provided as common, application search, just to show how to strtucture can be created in short time.
	   
	  Typical Angular 6 application written in Typescript. 
      SCSS is used here instead of plain css.
    	  
  If any suggestions do not hesitate and tell.
  
  v 1.0.8
    Added themes, reduces unneeded variables, refractored scss code
    
  v 1.0.7 
    Added Router paths to pages, added about, not found pages
  
  v 1.0.6
    Added Modular strucutre for vatsearch, added settings
  
  v 1.0.5 
    Added Modular structure for common components
  
  v 1.0.4
    Added reCaptcha v2
  
  v 1.0.3
   Added duplicate check for search results
   
  v 1.0.2
   Added loading bar and disable button when search process, undisables when search gets results
   
 
   ```
   


