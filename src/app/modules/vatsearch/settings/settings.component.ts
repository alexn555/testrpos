import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-search-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class VatSearchSettingsComponent implements OnInit {

  @Input() public isVisible: boolean;
  @Output() settingsApplied: EventEmitter<any> = new EventEmitter();
  public vatSearchSettingsForm: FormGroup;

  constructor(private fb: FormBuilder, private translate: TranslateService) {
    translate.setDefaultLang('en');
    translate.use('en');
  }

  ngOnInit(): void {
    this.createSearchSettingsForm();
  }

  createSearchSettingsForm(): void {
    this.vatSearchSettingsForm = this.fb.group({
        lastSearches: [true, [Validators.required]],
        upperCaseSearchText: [true, [Validators.required]]
      }
    );
    this.vatSearchSettingsForm = new FormGroup(this.vatSearchSettingsForm.controls, {
      updateOn: 'blur'
    });
  }

  sendSettingsUpdate(field: string, isChecked: boolean) {
    switch (field) {
      case 'last-searches':
        this.vatSearchSettingsForm.controls.lastSearches.setValue(isChecked);
        break;
      case 'upper-case-searchtext':
        this.vatSearchSettingsForm.controls.upperCaseSearchText.setValue(isChecked);
        break;
    }
    this.settingsApplied.emit(this.vatSearchSettingsForm.value);
  }


}
