import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';

import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { CommonModule } from '@common/common.module';

import { VatSearchComponent } from '@vatsearch/vatsearch.component';
import { VatSearchSettingsComponent } from './settings/settings.component';
import { VatSearchService } from '@vatsearch/vatsearch.service';

@NgModule({
  declarations: [
    VatSearchComponent,
    VatSearchSettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule,
    CommonModule
  ],
  exports: [
    VatSearchComponent,
    VatSearchSettingsComponent
  ],
  providers: [VatSearchService],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class VatSearchModule { }
