import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA} from '@angular/core';

import { BrowserModule } from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {TranslateModule} from '@ngx-translate/core';

import { AppHeaderComponent } from './header/header.component';
import { AppFooterComponent } from './footer/footer.component';
import { LoadingComponent } from './loading-bar/loading.component';
import { AppAboutComponent } from './about/about.component';
import { AppErrorPageComponent } from './error-page/error-page.component';

@NgModule({
  declarations: [
    LoadingComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AppAboutComponent,
    AppErrorPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    TranslateModule
  ],
  exports: [
    LoadingComponent,
    AppHeaderComponent,
    AppFooterComponent,
    AppAboutComponent,
    AppErrorPageComponent
  ],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class CommonModule { }
