export interface VatResult {
  country: string;
  name: string;
  address: string;
}

export interface VatResultRaw {
  countryCode: string;
  vatNumber: string;
  requestDate: string;
  valid: boolean;
  name: string;
  address: string;
}
