import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {VatSearchComponent} from '@vatsearch/vatsearch.component';
import {AppAboutComponent} from '@common/about/about.component';
import {AppErrorPageComponent} from '@common/error-page/error-page.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'vat-search',
    pathMatch: 'full'
  },
  {
    path: 'about',
    component: AppAboutComponent,
  },
  {
    path: 'vat-search',
    component: VatSearchComponent,
  },
  {
    path: '**',
    component: AppErrorPageComponent,
    data: {
      number: '404'
    }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
