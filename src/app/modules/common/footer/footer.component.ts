import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class AppFooterComponent implements OnInit {

  public footerContent: string;
  private turnOff: boolean;

  constructor() {
    this.turnOff = true;
  }

  ngOnInit(): void {
    this.footerContent = !this.turnOff ? '(c) 2019 Aleksandr Kalinin' : '';
  }

}
